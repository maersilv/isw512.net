
$(function () {
    $('#frm-prueba').form({
        nombre: {
            identifier: 'nombre',
            rules: [
                {
                type : 'empty',
                prompt : 'Por favor ingrese un nombre'
                }
            ]
        },
        apellido: {
            identifier: 'apellido',
            rules: [
                {
                type : 'empty',
                prompt : 'Por favor ingrese un apellido'
                }
            ]
        },
        email:{
            identifier: 'email',
            rules: [
                {
                type : 'email',
                prompt : 'Por favor ingrese un email valido'
                }
            ]
        },
        mes : {
            identifier: 'mes',
            rules: [
                {
                type : 'empty',
                prompt : 'Por favor seleccione una opción'
                }
            ]
        },
        password : {
            identifier: 'password',
            rules: [
                {
                type : 'minLength[8]',
                prompt : 'Por favor escriba una contraseña de al menos 8 caracteres'
                }
            ]
        },
        password2:{
            identifier: 'password2',
            rules: [
                {
                type : 'match[password]',
                prompt : 'Las contraseñas no coinciden'
                }
            ]
        }

    }, {
            onSucess: function (e) {
                e.preventDefault();
                alert('Enviando el formulario');
            }
        })
});